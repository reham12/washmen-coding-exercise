import "./assets/scss/style.scss";
import SearchOfPartners from "./Pages/SearchOfPartners";

function App() {
  return (
    <div className="App">
       <SearchOfPartners/>
    </div>
  );
}

export default App;

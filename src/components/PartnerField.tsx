import React, { ReactElement } from "react";

type FieldProps = Record<string, any> & {
  label: string;
  children: string;
  as?: any;
};

const PartnerField = ({
  label,
  children,
  as: As = "div",
  ...rest
}: FieldProps): ReactElement => {
  return (
    <As className="partner-field" {...rest}>
      <label className="partner-field-label">{label}:</label>
      <span className="partner-field-value">{children}</span>
    </As>
  );
};

export default PartnerField;

import { TextField } from "@material-ui/core";

interface FormSearchProps {
  setValueRange: any;
  valueRange: number | null;
  getPartnersInsideCircle: any;
}

const PartnerFormSearch = ({
  setValueRange,
  valueRange,
  getPartnersInsideCircle,
}: FormSearchProps) => {
  return (
    <div className="partners-form-search">
      <TextField
        onKeyPress={(e) => {
          if (e.key === "e" || e.key === "-") {
            e.preventDefault();
          }
        }}
        type="number"
        InputProps={{ inputProps: { min: 0 } }}
        label="Range"
        onChange={(e: any) => setValueRange(e.target.value)}
      />

      <input
        type="submit"
        className="submit-btn"
        onClick={() => {
          getPartnersInsideCircle(valueRange);
          console.log(valueRange);
        }}
      />
    </div>
  );
};

export default PartnerFormSearch;

import React from "react";
import { Col, Row } from "react-bootstrap";
import Logo from "../assets/icons/logo.svg";
import PartnerField from "./PartnerField";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMap } from "@fortawesome/free-solid-svg-icons";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { map } from "lodash";
const faBusinessTimeIcon = faMap as IconProp;

interface PartnerViewProps {
  partner: any;
}
const PartnerView = ({ partner }: PartnerViewProps) => {
  return (
    <div className="partner-view my-3">
      <Row className="align-items-center">
        <Col lg="3" sm="4" xs="12" className="partner-image-column">
          <div className="partner-image-container px-3">
            <img src={Logo} alt="WashMen_Logo" />
          </div>
        </Col>
        <Col lg="9" sm="8" xs="12" className="partner-item-info-column">
          <div className="partner-item-info pt-3">
            <PartnerField label={"Organization"}>
              {partner.organization}
            </PartnerField>
            <PartnerField label={"Customer Locations"}>
              {partner.customerLocations}
            </PartnerField>

            <PartnerField label={"Will Work Remotely"}>
              {partner.willWorkRemotely?"Yes":"No"}
            </PartnerField>
            <div className="partner-field">
              <label className="partner-field-label">Website:</label>
              <span className="partner-field-value">
                <a href={partner.website} target="_blank">{partner.website}</a>
              </span>
            </div>
          </div>
          <div className="partner-offices py-3">
            <div className="basic-name">Offices:</div>
            {map(partner.offices, (office) => (
              <div className="partner-office d-flex" key={office.coordinates}>
                <div className="mt-1">
                  <FontAwesomeIcon icon={faBusinessTimeIcon} />
                </div>

                <div className="mx-2">
                  <PartnerField label={"Location"}>
                    {office.location}
                  </PartnerField>
                  <PartnerField label={"Address"}>
                    {office.address}
                  </PartnerField>
                </div>
              </div>
            ))}
          </div>
          <div className="partner-item-hover p-3">
            <div className="partner-field d-none d-sm-block">
              <label className="partner-field-label">Website: </label>
              <span className="partner-field-value">
                <a href={partner.website} target="_blank">{partner.website}</a>
              </span>
            </div>
            <div>
              <div className="basic-name">Services:</div>
              {partner.services}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default PartnerView;

interface SvgIconProps {
  size?: string;
  icon: any;
  onClick?: any;
}
const SvgIcon = ({ icon, onClick, size, ...rest }: SvgIconProps) => {
  const Component = onClick ? "a" : "span";

  let sizeClassName = null;
  if (size) {
    sizeClassName =
      "Svg-Icon" +
      size
        .split("-")
        .reduce(
          (prev, curr) =>
            `${prev}-${curr.substr(0, 1).toUpperCase()}${curr.substr(1)}`,
          ""
        );
  }

  return (
    <Component
      href={Component == "a" ? "#" : undefined}
      className={"Svg-Icon-Container"}
      onClick={(e) => {
        if (onClick) {
          e.preventDefault();
          onClick(e);
        }
      }}
      {...rest}
    >
      <span className={"Svg-Icon"} dangerouslySetInnerHTML={{ __html: icon }} />
    </Component>
  );
};

export default SvgIcon;

import { useState } from "react";
import Partners from "../data/Partners.json";
import { map, orderBy, split } from "lodash";
import PartnerView from "../components/PartnerView";
import PartnerFormSearch from "../components/PartnerFormSearch";
import { COORDS_DEFAULT_VALUES } from "../constant";
import EmptyList from "../components/EmptyList";
import { Container } from "react-bootstrap";

const { greatCircleDistance } = require("great-circle-distance");
const SearchOfPartners = () => {
  // state
  const [valueRange, setValueRange] = useState(null);
  const [dataPartners, setDataPartners] = useState<any>([...Partners]);
  /**
   * action return partners inside special range
   * @param range
   */
  const getPartnersInsideCircle = (range: number | null) => {
    let new_partners: any = [];
    map(Partners, (item) => {
      let new_offices: any = [];
      map(item.offices, (office) => {
        let coords = {
          lat1: COORDS_DEFAULT_VALUES.lat1,
          lng1: COORDS_DEFAULT_VALUES.lng1,
          lat2: split(office.coordinates, ",")[0],
          lng2: split(office.coordinates, ",")[1],
        };

        var distanc = greatCircleDistance(coords);
        if (range && distanc <= range) {
          new_offices.push(office);

        }
      });
      if (new_offices.length > 0) {
        let new_item = { ...item };
        new_item["offices"] = new_offices;
        new_partners.push(new_item);
      }
    });
    new_partners=orderBy(new_partners, ["organization"], ["asc"]);
    setDataPartners([...new_partners]);
  };

  return (
    <Container className="partners-page py-4">
      <PartnerFormSearch
        valueRange={valueRange}
        setValueRange={setValueRange}
        getPartnersInsideCircle={getPartnersInsideCircle}
      />
      <div className="list-partners py-4">
        {dataPartners.length > 0 ? (
          map(dataPartners, (partner) => {
            return <PartnerView partner={partner} key={partner.id} />;
          })
        ) : (
          <EmptyList />
        )}
      </div>
    </Container>
  );
};

export default SearchOfPartners;
